import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import Axios from "axios";
import LoadingDotsIcon from "./LoadingDotsIcon";

function ProfileFollowing() {
  const [isLoading, setIsLoading] = useState(true);
  const [posts, setPosts] = useState([]);
  const { username } = useParams();

  useEffect(() => {
    //use effect is watching for something to change, use state gives us a current state and a function to modify the state
    async function fetchPosts() {
      try {
        const response = await Axios.get(`/profile/${username}/following`);
        setPosts(response.data);
        //console.log(response.data);
        setIsLoading(false);
      } catch (e) {
        console.log(e.response.data);
      }
    }
    fetchPosts();
  }, [username]);

  if (isLoading) {
    return <LoadingDotsIcon />;
  } else {
    return (
      <>
        <div className="list-group">
          {posts.map((follower, index) => {
            return (
              <Link
                key={index}
                to={`/profile/${follower.username}`}
                className="list-group-item list-group-item-action"
              >
                <img className="avatar-tiny" src={follower.avatar} />{" "}
                {follower.username}
              </Link>
            );
          })}
        </div>
      </>
    );
  }
}

export default ProfileFollowing;
