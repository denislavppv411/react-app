import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import Axios from "axios";
import LoadingDotsIcon from "./LoadingDotsIcon";
import Post from "./Post";

function ProfilePosts() {
  const [isLoading, setIsLoading] = useState(true);
  const [posts, setPosts] = useState([]);
  const { username } = useParams();

  useEffect(() => {
    //use effect is watching for something to change, use state gives us a current state and a function to modify the state
    async function fetchPosts() {
      try {
        const response = await Axios.get(`/profile/${username}/posts`);
        setPosts(response.data);
        //console.log(response.data);
        setIsLoading(false);
      } catch (e) {
        console.log(e.response.data);
      }
    }
    fetchPosts();
  }, [username]);

  if (isLoading) {
    return <LoadingDotsIcon />;
  } else {
    return (
      <>
        <div className="list-group">
          {posts.map((post) => {
            return <Post noAuthor={true} post={post} key={post._id} />;
          })}
        </div>
      </>
    );
  }
}

export default ProfilePosts;
