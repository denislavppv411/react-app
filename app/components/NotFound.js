import React, { useEffect } from "react";
import Page from "./Page";
import { Link } from "react-router-dom";

function NotFound() {
  return (
    <Page title="Not Found">
      <div className="text-center">
        <h2>Not found. 404</h2>
        <p className="lead text-muted">
          You can go back to <Link to="/">homepage</Link>
        </p>
      </div>
    </Page>
  );
}

export default NotFound;
