import React, { useEffect, useState, useContext } from "react";
import { useParams, Link } from "react-router-dom";
import Axios from "axios";
import LoadingDotsIcon from "./LoadingDotsIcon";
import StateContext from "../StateContext";

function ProfileFollowers(props) {
  const appState = useContext(StateContext);
  const { username } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    //use effect is watching for something to change, use state gives us a current state and a function to modify the state
    async function fetchPosts() {
      try {
        const response = await Axios.get(`/profile/${username}/followers`);
        setPosts(response.data);
        //console.log(response.data);
        setIsLoading(false);
      } catch (e) {
        console.log(e.response.data);
      }
    }
    fetchPosts();
  }, [username]);

  if (isLoading) return <LoadingDotsIcon />;
  //console.log(posts.length);
  return (
    <div className="list-group">
      {posts.length > 0 &&
        posts.map((follower, index) => {
          return (
            <>
              {console.log(follower.username)}
              <Link
                key={index}
                to={`/profile/${follower.username}`}
                className="list-group-item list-group-item-action"
              >
                <img className="avatar-tiny" src={follower.avatar} />{" "}
                {follower.username}
              </Link>
            </>
          );
        })}
      {posts.length == 0 && appState.user.username == username && (
        <>
          <p className="lead text-muted text-center">
            You don't have any followers yet.
          </p>
        </>
      )}
      {posts.length == 0 && appState.user.username != username && (
        <>
          <p className="lead text-muted text-center">
            {username} doesn't have any followers yet.
            {appState.loggedIn && "Be the first to follow them!"}
            {!appState.loggedIn && (
              <>
                {" "}
                If you want to follow them you need to{" "}
                <Link to="/">sign up</Link> for an account first.{" "}
              </>
            )}
          </p>
        </>
      )}
    </div>
  );
}

export default ProfileFollowers;
