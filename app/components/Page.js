import React, { useEffect } from "react";
import Container from "./Container";

function Page(props) {
  useEffect(() => {
    document.title = `${props.title} | CustomApp`;
    window.scrollTo(0, 0);
  }, [props.title]); // only the first time is rendered
  return <Container wide={props.wide}>{props.children}</Container>;
}

export default Page;
